<?php
namespace Database\Seeders;
use Illuminate\Database\Seeder;
use DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
 
        DB::table('users')->insert([
                        
            ['name' => 'admin',  'email' =>'admin@admin.com' , 'email_verified_at' =>'2020-09-10 14:20:08' ,'password' => bcrypt('password') ]
          
        ]);


    }
}
