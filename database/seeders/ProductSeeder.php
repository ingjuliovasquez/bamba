<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('products')->insert([
            ['id' => 1, 'sku'  => 'SKU01',  'price'  => 125.50],
            ['id' => 2, 'sku'  => 'SKU02',  'price'  => 210.00],
            ['id' => 3, 'sku'  => 'SKU03',  'price'  => 50.50],
            ['id' => 4, 'sku'  => 'SKU04',  'price'  => 15.50]
        ]);
    }
}
