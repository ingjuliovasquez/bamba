<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('orders')->insert([
            ['id' => 1, 'total' => '200.00'],
            ['id' => 2, 'total' => '300.00'],
            ['id' => 3, 'total' => '400.00'],
            ['id' => 4, 'total' => '500.00'],
            ['id' => 5, 'total' => '600.00'],
            ['id' => 6, 'total' => '700.00'],
        ]);
    }
}
