<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    public $table = 'orders';
    public $fillable = [
        'id',
        'total',

    ];


    public function orders_items() {
        return $this->hasMany(Item::class, 'order_id', 'id');
    }


    
}
