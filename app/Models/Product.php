<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    public $table = 'products';
    public $fillable = [
        'id',
        'sku',
        'price'

    ];

    public function orders_items() {
        return $this->hasMany(Product::class, 'product_id', 'id');
    }

}
