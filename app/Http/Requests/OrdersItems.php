<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrdersItems extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order_id' => 'required',
            'product_id' => 'required',
            'quantity' => 'required',
            'total' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'order_id.required' => 'Por favor seleccione una orden',
            'product_id.required' => 'Por favor seleccione un producto',
            'quantity.required' => 'Por favor ingrese una cantidad',
            'total.required' => 'Por favor ingrese el total',
        ];
    }
}
