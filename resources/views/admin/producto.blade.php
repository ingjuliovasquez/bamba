
@extends('template.admin')

@section('content')
<!-- Start content -->
      <!-- Start right Content here -->
            <!-- ============================================================== -->

                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="page-title-box">
                            <div class="row align-items-center">
                                <div class="col-sm-6">
                                    <h4 class="page-title">Solicitudes</h4>
                                </div>
                                <div class="col-sm-6">
                                    <ol class="breadcrumb float-right">
                                     
                                        <li>
                                            <a href="{{ route('logout') }}"
                                               onclick="event.preventDefault();
                                               document.getElementById('logout-form').submit();">
                                            
                                               <H3>Cerrar sesión</H3> 
                                            </a>
                            
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                        </li>
                                    </ol>
                                </div>
                            </div> <!-- end row -->
                        </div>
                        <!-- end page-title -->
                    </div>

                    

    <div class="col-10">
        <div class="pull-right pb-2">            
            <a href="{{ route('admin') }}" class="btn btn-outline-primary " title="Agregar Videos" ><span class="btn-label"><H3> INICIO</H3></span></a>            
        </div>
        <br><br>
        <div class="card m-b-30">
            <div class="card-body ">

                <h4 class="mt-0 header-title">Nueva Solicitud</h4>
                {{-- <p class="sub-title">Input masks can be used to force the
                    user to enter data conform a specific format. Unlike validation, the
                    user can't enter any other key than the ones specified by the mask.
                </p> --}}
                <br>
                <form enctype="multipart/form-data" id="solicitud">
                    @csrf
                <div class="row  ">
                    <div class="col-lg-6">
                        <div class="p-10">
                                  
                                <div class="form-group">
                                    <label>Nombre</label>
                                    <input type="text" placeholder="" name="nombre"  id="nombre" class="form-control ">
                                    
                              
                                </div>
                                <div class="form-group">
                                    <label>Primer Apellido</label>
                                    <input type="text" placeholder="" name="primer_apellido"  id="primer_apellido" class="form-control ">
                                    
                              
                                </div>
                                <div class="form-group">
                                    <label>Segundo Apellido</label>
                                    <input type="text" placeholder="" name="segundo_apellido"  id="segundo_apellido" class="form-control ">
                                    
                              
                                </div>
                                <div class="form-group">
                                    <label>Dirección</label>
                                    <input type="text" placeholder="" name="direccion"  id="direccion" class="form-control ">
                                    
                              
                                </div>
                                <div class="form-group">
                                    <label>Telefono</label>
                                    <input type="tect" placeholder="" name="telefono"  id="telefono" class="form-control ">
                                    
                              
                                </div>
                              
                                  
                             
                              
                          
                            
               

                            </div> <!-- end row -->

                      
                            </div> <!-- end row -->
                            <div class="col-lg-6">
                                <div class="p-10">
                            <div class="form-group">

                                <label>CURP</label>
                                <input type="text" placeholder="" name="curp"  id="curp" class="form-control ">
                                                          
                            </div>

                            <div class="form-group">

                                <label>NSS</label>
                                <input type="text" placeholder="" name="nss"  id="nss" class="form-control ">
                                                          
                            </div>

                            <div class="form-group">

                                <label>Credito</label>
                                <input type="text" placeholder="" name="credito"  id="credito" class="form-control ">
                                                          
                            </div>
                            
                        </div>
                        <button type="button" class="btn btn-primary waves-effect waves-light guardar " id="guardar">
                            Guardar
                        </button>
                    </div>
                </div> <!-- end row -->
            </form>
            </div>
       
        </div>
    </div> <!-- end col -->



                </div>    
            
            <!-- ============================================================== -->
            <!-- End Right content here -->
<!-- content -->
@endsection
@section('scripts')

<script src="{{ asset('js/solicitudes.js')}}"></script>
@endsection