@extends('layouts.app')

@section('content')
<div class="container">
    <div class="d-flex justify-content-between align-items-end mb-3">
        <h3 class="m-0">Lista de items</h3>
        <a href="/orders-items/create" class="btn btn-primary">Agregar</a>
    </div>
    <div class="card p-5">
        <table class="table table-striped table-hover">
            <thead>
                <th>Orden</th>
                <th>Producto</th>
                <th>Cantidad</th>
                <th>Total</th>
            </thead>
            <tbody>
                <tr>
                    <td>#55A</td>
                    <td>Carne</td>
                    <td>10</td>
                    <td>$1.000</td>
                </tr>
                <tr>
                    <td>#55A</td>
                    <td>Carne</td>
                    <td>10</td>
                    <td>$1.000</td>
                </tr>
                <tr>
                    <td>#55A</td>
                    <td>Carne</td>
                    <td>10</td>
                    <td>$1.000</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
@endsection