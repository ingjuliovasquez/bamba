<div class="topbar">

    <!-- LOGO -->
    <div class="topbar-left">
        <a href="/cursos" class="logo">
            <span class="logo-light">
                <i class="mdi mdi-camera-control"></i> Solicitudes
            </span>
            <span class="logo-sm">
                <i class="mdi mdi-camera-control"></i>
            </span>
        </a>
    </div>

    <nav class="navbar-custom">
        <ul class="navbar-right list-inline float-right mb-0">
            

            <!-- full screen -->
            <li class="dropdown notification-list list-inline-item d-none d-md-inline-block">
                <a class="nav-link waves-effect" href="#" id="btn-fullscreen">
                    <i class="mdi mdi-arrow-expand-all noti-icon"></i>
                </a>
            </li>


       

        </ul>

        <ul class="list-inline menu-left mb-0">
       
      
        </ul>

    </nav>

</div>