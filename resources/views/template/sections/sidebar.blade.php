<div class="left side-menu">
    <div class="slimscroll-menu" id="remove-scroll">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu" id="side-menu">
                <li class="menu-title">Menu</li>
                <li>
                    <a href="javascript:void(0);" class="waves-effect">
                        <i class="icon-accelerator"></i><span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span> <span> HOTEL </span>
                    </a>
                    <ul class="submenu">
                        <li><a href="{{route('index.admin')}}">Alta Producto</a></li>
                 
                    </ul>
                </li>
         
                {{-- <li>
                    <a href="{{route('index.hotel')}}" class="waves-effect ">
                        <i class="icon-accelerator"></i>
                        <span> CLIENTES </span>
                    </a>
                </li> --}}
                <li>
                    <a href="{{route('admin.empleado.index')}}" class="waves-effect">
                        <i class="icon-diamond"></i><span class="badge badge-success badge-pill float-right"></span> <span> USUARIOS </span>
                    </a>
                </li>
                <li>
                    <a href="{{route('index.huesped')}}" class="waves-effect ">
                        <i class="icofont-ui-video-play"></i>
                        <span> USUARIOS </span>
                    </a>
           
                </li>
            
                <li>
                    <a href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();">
                       <i class="icon-share"></i>
                       <span> Cerrar sesión</span>
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
                {{-- <li>
                    <a href="" class="waves-effect">
                        <i class="icon-accelerator"></i><span class="badge badge-success badge-pill float-right"></span> <span> Agenda </span>
                    </a>
                </li> --}}
                {{-- <li>
                    <a href="javascript:void(0);" class="waves-effect"><i class="icon-mail-open"></i><span> Email <span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span> </span></a>
                    <ul class="submenu">
                        <li><a href="email-inbox.html">Inbox</a></li>
                        <li><a href="email-read.html">Email Read</a></li>
                        <li><a href="email-compose.html">Email Compose</a></li>
                    </ul>
                </li> --}}

                <li>
                    {{-- <a href="calendar.html" class="waves-effect"><i class="icon-calendar"></i><span> Calendar </span></a> --}}
                </li>

                {{-- <li>
                    <a href="javascript:void(0);" class="waves-effect"><i class="icon-paper-sheet"></i><span> Pages <span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span> </span></a>
                    <ul class="submenu">
                        <li><a href="pages-pricing.html">Pricing</a></li>

                    </ul>
                </li>

                <li class="menu-title">Vistas</li>

                <li>
                    <a href="javascript:void(0);" class="waves-effect"><i class="icon-pencil-ruler"></i> <span> ferds <span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span> </span> </a>
                    <ul class="submenu">
                        <li><a href="ui-alerts.html">Alerts</a></li>
                        <li><a href="ui-badge.html">Badge</a></li>

                    </ul>
                </li>

                <li>
                    <a href="javascript:void(0);" class="waves-effect"><i class="icon-diamond"></i> <span> Advanced UI <span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span> </span> </a>
                    <ul class="submenu">
                        <li><a href="advanced-alertify.html">Alertify</a></li>
                        <li><a href="advanced-rating.html">Rating</a></li>

                    </ul>
                </li>

                <li>
                    <a href="javascript:void(0);" class="waves-effect"><i class="icon-graph"></i><span> Charts <span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span> </span></a>
                    <ul class="submenu">
                        <li><a href="charts-morris.html">Morris Chart</a></li>

                    </ul>
                </li> --}}


            </ul>

        </div>
        <!-- Sidebar -->
        <div class="clearfix"></div>

    </div>
    <!-- Sidebar -left -->

</div>
