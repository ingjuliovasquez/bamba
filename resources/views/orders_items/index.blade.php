@extends('layouts.app')

@section('content')
<div class="container">
    <div class="d-flex justify-content-between align-items-end mb-3">
        <h3 class="m-0">Orders Items</h3>
        <a href="/orders-items/create" class="btn btn-primary">Agregar</a>
    </div>
    <div class="card p-5">
        <table class="table table-striped table-hover">
            <thead>
                <th>Valor Unitario</th>
                <th>Sku</th>
                <th>Cantidad</th>
                <th>Total</th>
                <th class="text-center">Opciones</th>
            </thead>
            <tbody>
                @foreach($items as $index => $item)
                <tr>
                    <td>${{ $item->order->total }}</td>
                    <td>{{ $item->product->sku }}</td>
                    <td>{{ $item->quantity }}</td>
                    <td>${{ $item->total }}</td>
                    <td class="text-center d-flex alingn-items-center justify-content-center">
                        <a href="/orders-items/{{ $item->id }}/edit" class="text-primary mr-1 d-flex alingn-items-center">
                            <i class="far fa-edit"></i>
                        </a>
                        <form action="/orders-items/{{ $item->id }}" method="POST">
                            @csrf
                            @method('DELETE')

                            <button class="text-danger btn btn-link m-0 p-0 d-flex alingn-items-center">
                                <i class="far fa-trash-alt"></i>
                            </button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection