@extends('layouts.app')

@section('content')
<div class="container">
    <h3 class="mb-3">Registro de items</h3>

    <div class="card p-5">
        <form action="/orders-items/{{ $item->id }}" method="POST">
            @csrf
            @method('PUT')

            <div class="mb-3">
                <label>Orden*</label>
                <select name="order_id" class="form-control">
                    <option value="">Seleccione una orden</option>

                    @foreach($orders as $order)
                    @if($order->id == $item->order_id)
                    <option value="{{ $order->id }}" selected>{{ $order->total }}</option>
                    @else
                    <option value="{{ $order->id }}">{{ $order->total }}</option>
                    @endif
                    @endforeach
                </select>
                @error('order_id')
                <span class="text-danger">{{ $message }}</span>
                @enderror
            </div>
            <div class="mb-3">
                <label>Producto*</label>
                <select name="product_id" class="form-control">
                    <option value="">Seleccione un producto</option>

                    @foreach($products as $product)
                    @if($product->id == $item->product_id)
                    <option value="{{ $product->id }}" selected>{{ $product->sku }}</option>
                    @else
                    <option value="{{ $product->id }}">{{ $product->sku }}</option>
                    @endif
                    @endforeach
                </select>
                @error('product_id')
                <span class="text-danger">{{ $message }}</span>
                @enderror
            </div>
            <div class="row">
                <div class="col-6">
                    <label>Cantidad*</label>
                    <input type="number" name="quantity" class="form-control" value="{{ $item->quantity }}">
                    @error('quantity')
                    <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
                <div class="col-6">
                    <label>Total*</label>
                    <input type="text" name="total" class="form-control" value="{{ $item->total }}">
                    @error('total')
                    <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="text-right mt-5">
                <a href="/orders-items" class="btn btn-danger">Cancelar</a>
                <button type="submit" class="btn btn-primary">Actualizar</button>
            </div>
        </form>
    </div>
</div>
@endsection