@extends('layouts.app')

@section('content')
<div class="container">
    <h3 class="mb-3">Registro de items</h3>

    <div class="card p-5">
        <form action="/orders-items" method="POST">
            @csrf

            <div class="mb-3">
                <label>Orden*</label>
                <select name="order_id" class="form-control">
                    <option value="">Seleccione una orden</option>

                    @foreach($orders as $order)
                    <option value="{{ $order->id }}">${{ $order->total }}</option>
                    @endforeach
                </select>
                @error('order_id')
                <span class="text-danger">{{ $message }}</span>
                @enderror
            </div>
            <div class="mb-3">
                <label>Producto*</label>
                <select name="product_id" class="form-control">
                    <option value="">Seleccione un producto</option>

                    @foreach($products as $product)
                    <option value="{{ $product->id }}">{{ $product->sku }}</option>
                    @endforeach
                </select>
                @error('product_id')
                <span class="text-danger">{{ $message }}</span>
                @enderror
            </div>
            <div class="row">
                <div class="col-6">
                    <label>Cantidad*</label>
                    <input type="number" name="quantity" class="form-control">
                    @error('quantity')
                    <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
                <div class="col-6">
                    <label>Total*</label>
                    <input type="text" name="total" value="500.00" class="form-control">
                    @error('total')
                    <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="text-right mt-5">
                <a href="/orders-items" class="btn btn-danger">Cancelar</a>
                <button type="submit" class="btn btn-primary">Registrar</button>
            </div>
        </form>
    </div>
</div>
@endsection